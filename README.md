# Gsper
Ǧ'Perdu l'accès à mon compte, Ǧ'spère le retrouver !

### [Ǧ'vais essayer maintenant !](https://g1.1000i100.fr/gsper/)
Ǧ'vais essayer hors-ligne : télécharger Ǧsper en [zip](https://g1.1000i100.fr/gsper/gsper-offline.zip) ou [tgz](https://g1.1000i100.fr/gsper/gsper-offline.tgz).

## Principe
- [x] Gsper test pour vous les combinaisons d'identifiants secrets et de mot de passe
et vous signale si une des correspondances correspond au compte recherché.
- [x] Il génère aussi des variantes issues des mots que vous lui fournissez
pour vous donner plus de changes de trouver la bonne combinaison.
- [x] Il affiche le nombre de combinaisons à tester, et le temps estimé pour les essayer toutes.

En sécurité informatique ce que des outils tels que Ǧsper font est appelé [attaque par dictionnaire](https://fr.wikipedia.org/wiki/Attaque_par_dictionnaire).

## Financer le développement

Le 11/04/2018 j'avais créé [un compte dédié](g1:22TTh6fD8uEC9CYq5sB8QpqjyXEHH5QnAiEdy68D2Y7S) pour Ǧsper.
Mais les années sont passées et je n'ai plus la moindre idée d'où sont les accès correspondants.
J'ai tenté d'utiliser Ǧsper pour retrouver l'accès, mais sans succès à ce jour, comme quoi, un compte bien sécurisé
et pas ou peu d'indices sur ce qu'était les secrets de connexion ne permet pas d'accéder à un compte.

Bref, en attendant mieux, pour soutenir mes implications dans la Ǧ1 mon compte membre sera une meilleure destination :

👤 : 1000i100 🔑 : 2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT

## Guide d'utilisation

Dans le dossier [docs/](docs/) se trouve :
- [Le guide d'utilisation détaillant toutes les syntaxes reconnue pour générer des variantes](docs/doc.fr.adoc)
- [Le guide indiquant les bonnes pratiques de sécurité](docs/secu.fr.adoc)
- [Et quelques exemples](docs/exemples.fr.adoc)
