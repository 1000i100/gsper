import {idSecPass2cleanKeys} from "g1lib/browser/crypto.mjs";
import {Dictionary} from "g1lib/browser/dictionary.mjs";
import staticPreview from "./logic/staticPreview.mjs";

const prioritizedTaskQueue = {gracefulStop:[],clearQueue:[],fastPreview:[],smartPreview:[],bruteForce:[],batchBruteForce:[]};
let idle=0;
const run = {};
run.gracefulStop = ()=>self.stop();
run.clearQueue = clearQueue;
run.sleep = sleep;
run.fastPreview = preview;
run.smartPreview = preview;
run.bruteForce = async (data)=>{
  const keys = await idSecPass2cleanKeys(data.idSec,data.pass);
  postMessage({...data,...keys});
}
run.batchBruteForce = async (data)=>{
  const batch = await Promise.all(data.batch.map(async d=> {return {...d,...(await idSecPass2cleanKeys(d.idSec,d.pass))};} ))
  postMessage({...data,batch});
}
async function sleep(data) {
  return new Promise((resolve) => setTimeout(resolve, data.ms));
}
function preview(data) {
  try{
    data.dico = new Dictionary(data.secrets, data.options);
    staticPreview(data);
    data.dico = JSON.stringify(data.dico);
    postMessage(data);
  } catch (e){
    console.error(e);
    data.action = 'throw';
    data.message = e.message;
    postMessage(data);
  }
}
function clearQueue(){
  Object.keys(prioritizedTaskQueue).forEach(key=>prioritizedTaskQueue[key] = []);
}


async function queueLoop(){
  // choose task
  let chosen;
  if(prioritizedTaskQueue.gracefulStop.length) chosen = prioritizedTaskQueue.gracefulStop.pop();
  else if(prioritizedTaskQueue.fastPreview.length) {
    chosen = prioritizedTaskQueue.fastPreview.pop();
    prioritizedTaskQueue.fastPreview = [];
  } else if(prioritizedTaskQueue.smartPreview.length) {
    chosen = prioritizedTaskQueue.smartPreview.pop();
    prioritizedTaskQueue.smartPreview = [];
  } else if(prioritizedTaskQueue.bruteForce.length) chosen = prioritizedTaskQueue.bruteForce.shift();
  else if(prioritizedTaskQueue.batchBruteForce.length) chosen = prioritizedTaskQueue.batchBruteForce.shift();
  else chosen = {action:'sleep',ms:Math.min(500,idle*50)};
  if(chosen.action==='sleep') idle++;
  else idle=0;
  // await running task
  await run[chosen.action](chosen);
  // loop
  queueLoop();
}

// Init worker
self.addEventListener('message', async function(event){
  prioritizedTaskQueue[event.data.action].push(event.data);
});
queueLoop();
