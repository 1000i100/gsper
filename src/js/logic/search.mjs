import {resetWorkers, sendToWorker, subscribe} from "./workerManager.mjs";
import {pubKey} from "../ux/1_pubKey.mjs";
import getDico, {genDico} from "./dico.mjs";
import getCombiPerSec from "./speedBench.mjs";

export let searchRunning = false;
export let tested = 0;
export let departTime=0;
export let endTime;
export let found;
export let cacheActive = true;
export const DEFAULT_CACHE_MAX = 1_000_000;
export const cacheScale = [];

const globalCache = {};

export function duplicateCount() {
  return Object.keys(globalCache).reduce((acc, cur) => acc + Math.max(0, Object.keys(globalCache[cur]).length - 1), 0);
}

export async function startSearch() {
  const batchSize = Math.max(1,Math.round(getCombiPerSec()/navigator.hardwareConcurrency));
  await sleep(300);
  departTime = Date.now();
  tested = 0;
  let dico;
  try {
    dico = getDico();
  } catch (e) {
    dico = genDico();
  }
  searchRunning = true;
  wakeLock();
  document.addEventListener('visibilitychange', wakeLock);
  let index = 0;
  let batch = [];
  while (searchRunning && index < dico.length) {
    const {idSec, pass} = dico.splitGet(index);
    let cacheKey = `${idSec}@@${pass}`;
    if (cacheActive && (globalCache[cacheKey] || cacheKey.includes('§duplicate§'))) {
      cacheKey = cacheKey.replace(/§duplicate§/g, '');
      if (!globalCache[cacheKey]) globalCache[cacheKey] = {};
      globalCache[cacheKey][index]=true;
      tested++;
      index++;
      continue;
    }
    batch.push({idSec, pass, index});
    if(batch.length===batchSize){
      await sendToWorker('batchBruteForce',{batch});
      batch = [];
    }
    index++;
  }
  if(batch.length) await sendToWorker('batchBruteForce',{batch});
  if (tested >= dico.length) endSearchTest();
}

export function stopSearch() {
  searchRunning = false;
  releaseWakeLock();
  resetWorkers();
}
function updateBatchProgress(data) {
  if (data.bench) return;
  data.batch.forEach(d=>updateProgress(d));
}
function updateProgress(data) {
  if (data.bench) return;
  tested++;
  if(found) endSearchTest();
  if (data.publicKey === pubKey) {
    endSearchTest(data);
  } else {
    if (cacheActive && !data.bench) {
      const cacheKey = `${data.idSec}@@${data.pass}`;
      if (!globalCache[cacheKey]) globalCache[cacheKey] = {};
      globalCache[cacheKey][data.index] = true;
    }
    if (Object.keys(globalCache).length > getDico().config.cacheMax) cacheActive = false;
  }
  if (tested >= getDico().length) endSearchTest();
}

function endSearchTest(somethingFound = undefined) {
  endTime = Date.now();
  stopSearch();
  if (!found) found = somethingFound;
  window.location = '#page_4_result';
}

subscribe('batchBruteForce', updateBatchProgress);

async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

let wakeLockHandler;
async function wakeLock(){
  if(searchRunning && 'wakeLock' in navigator && document.visibilityState === 'visible') {
    try {
      wakeLockHandler = await navigator.wakeLock.request('screen');
    } catch (err) {}
  }
}
async function releaseWakeLock(){
  document.removeEventListener('visibilitychange', wakeLock);
  if('wakeLock' in navigator && wakeLockHandler){
    await wakeLockHandler.release();
    wakeLockHandler = null;
  }
}
