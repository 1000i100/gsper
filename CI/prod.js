const fs = require("fs");
change(
	`<script type="module" src="main.js"></script>`,
	`<script src="main.js"></script>`,
	`generated.public/index.html`);
change(
	`Worker('worker.js',{type:"module"})`,
	`Worker('worker.js')`,
	`generated.public/main.js`);

function change(thisString,withThisString,inThisFile) {
	try{fs.writeFileSync(inThisFile,(fs.readFileSync(inThisFile,"utf8"))
			.replace(thisString,withThisString)
		,"utf8");} catch (e) {console.error(e);}

}
