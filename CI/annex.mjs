import {readdirSync,readFileSync,writeFileSync} from "fs";
import ejs from "ejs";

async function main(){
  const inputPath = 'generated/tmp/';
  readdirSync(inputPath).map(f=>{
    const content = `
<%- include('src/pages/fragments/head.ejs'); %>
<%- include('src/pages/fragments/annex.ejs'); %>
${readFileSync(inputPath+f,'utf8')}
<%- include('src/pages/fragments/footer.ejs'); %>
`;
    writeFileSync(`generated/public/${f}`, ejs.render(content,{},{filename:f,root:'./'}));
  });
}
main();
