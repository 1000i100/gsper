const fs = require("fs");
try{fs.writeFileSync("generated.public/vendors/nacl.js",(fs.readFileSync("node_modules/tweetnacl/nacl-fast.js","utf8"))
		.replace("(function(nacl) {","var nacl = {};")
		.replace("})(typeof module !== 'undefined' && module.exports ? module.exports : (self.nacl = self.nacl || {}));","export default nacl;")
	,"utf8");} catch (e) {console.error(e);}

try{fs.writeFileSync("generated.public/vendors/scrypt.js",(fs.readFileSync("node_modules/scrypt-async-modern/dist/index.js","utf8"))
		.replace("exports.default = scrypt;","export default scrypt;")
		.replace(`Object.defineProperty(exports, "__esModule", { value: true });`,"")
	,"utf8");} catch (e) {console.error(e);}

