[EN](CHANGELOG.md) | FR

# Changelog | liste des changements
Tous les changements notables de ce projet seront documenté dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnage sémantique](https://semver.org/spec/v2.0.0.html).

## Evolutions probable :
- PWA (site convertible en application mobile utilisable hors ligne)

## [Non-publié/Non-Stabilisé] (par [Ludovic Gros])

### Ajouté

##### Nouveau desgin
- Page d'accueil
- Page clef publique
- Page saisie des secrets
  - Volet explicatif des fonctions Majuscule Désaccentuer et Avancé
- Page avancement recherche
- Page résultat
- Fil d'ariane navigable

##### Interface responsive
- Design et ergonomie adapté au mobile

## [Version 2.2.1] - 2020-07-02 (par [1000i100])

### Ajouté
- Fichier LICENCE (AGPL-3.0)
- Fichier CHANGELOG.fr.md
- Fichier CHANGELOG.md

## [Version 2.2.0] - 2018-09-14 (par [1000i100])

### Ajouté
- exemple pas à pas du scénario d'utilisation
- si aucune clef publique n'est fournie, un message d'erreur est affiché.

### Modifié
- Retrait de la mention module, superflue après build (meilleur compatibilité)

## [Version 2.1.0] - 2018-06-27 (par [1000i100])

### Ajouté
- syntaxe =référence> pour faire des références syncronisé et éviter de générer des variantes non souhaitées.
- Génération d'une version utilisable hors ligne (meilleur sécurité).

##### DunikeyMaker ajouté en vrac
- afficher clef publique et privé à partir de ses secrets
- créer un fichier d'authentification contenant ses secrets

## [Version 2.0.0] - 2018-05-10 (par [1000i100])

### Ajouté
##### Design / ergonomie
- invitation au don
- lien vers la doc
- titres explicatifs
##### Générateur de variantes de mot de passe
- Déclinaisons avec Majuscules
- Désaccentuation
- Déclinaison avancées façon expression régulière
##### Documentation
- Guide d'utilisation
- Rédaction d'une documentation des générateur de variante de mot de passe
##### Améliorations technique
- Utilisation de Web-Worker (meilleur ergonomie et performance).
- Ajout de test unitaire (meilleur fiabilité).
- Différentiation de la lib pour la partie crypto et de celle de génération de variantes (meilleur maintenabilité et évolutivité).


### Modifié
- Amélioration drastique des performance (grace au webworker notament)
- Export du CSS en feuille de style séparée


## [Version 1.0.1 (Proof of Concept)] - 2018-04-18 (par [1000i100])
### Ajouté

##### Interface unifiée avec
- saisie de la clef publique,
- des identifiants secrets et mots de passe
- nombre de combinaison à tester
- estimatif temporel et d'avancement
- bouton pour cherche la combinaison valide
- lien vers le code source
##### Algorithme
- intégration des librairies de crypto nécessaires
- calcul de la clef publique correspondant à chaque combinaison de secrets saisie, et comparaison à la clef publique de référence.
##### Intégration continue
- publication automatisé de Gsper sous forme de gitlab-pages.

[Non-publié/Non-Stabilisé]: https://framagit.org/1000i100/gsper/-/compare/v2.2.1...master

[Version 2.2.1]: https://framagit.org/1000i100/gsper/-/compare/v2.2.0...v2.2.1
[Version 2.2.0]: https://framagit.org/1000i100/gsper/-/compare/v2.1.0...v2.2.0
[Version 2.1.0]: https://framagit.org/1000i100/gsper/-/compare/v2.0.0...v2.1.0
[Version 2.0.0]: https://framagit.org/1000i100/gsper/-/compare/v1.0.1...v2.0.0
[Version 1.0.1 (Proof of Concept)]: https://framagit.org/1000i100/gsper/-/tree/v1.0.1

[1000i100]: https://framagit.org/1000i100 "@1000i100"